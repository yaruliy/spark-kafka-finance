package yaruliy.source
import yaruliy.util.Util

class OandaDataSource extends DataSource{
  @throws(classOf[java.io.IOException])
  override def data: String = {
    val builder = StringBuilder.newBuilder
    var first: Boolean = true
    val iterator = scala.io.Source.fromURL(prepareURL).getLines()

    while(iterator.hasNext){
      if(first) {
        iterator.next()
        first = false
      }
      builder.append(iterator.next()).append(";")
    }
    builder.toString()
  }

  def prepareURL: String = {
    var result: String = "https://www.oanda.com/rates/api/v1/rates/" +
      Util.getPropertie("baseCurrency") +
      ".csv?api_key=a7C6lhtwP1styVH2FmsLpriA&" +
      "decimal_places=5&" +
      "date=2017-07-03&" +
      "fields=averages&"
    val currencys = Util.getPropertie("currencys").split(",")
    for(i <- 0 to (currencys.length-2)){
      result = result + "quote=" + currencys(i) + "&"
    }
    result = result + "quote=" + currencys(currencys.length-1)
    result
  }
}
