package yaruliy.source

class YahooDataSource extends DataSource{
  private val url: String =
    "https://www.finance.yahoo.com/rates/api/v1/rates/USD.csv" +
      "?api_key=c2x4hJU1eARSU1v2KdircF7b" +
      "&decimal_places=5" +
      "&date=2017-06-22" +
      "&fields=averages" +
      "&quote=UAH&" +
      "quote=MNT&" +
      "quote=BTC"
  override def data: String = {
    val data = scala.io.Source.fromURL(url).mkString
    data
  }
}