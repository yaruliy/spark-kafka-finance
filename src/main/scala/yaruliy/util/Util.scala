package yaruliy.util
import java.util.Properties
import scala.io.Source

object Util {
  private val properties: Properties = new Properties()
  def getPropertie(param: String): String = {
    val url = getClass.getResource("/dataConf.properties")
    properties.load(Source.fromURL(url).bufferedReader())
    properties.getProperty(param)
  }
}