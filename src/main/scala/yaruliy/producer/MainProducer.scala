package yaruliy.producer
import java.util.Properties
import yaruliy.util.Util
import org.apache.kafka.clients.producer._
import yaruliy.source.{DataSource, OandaDataSource, YahooDataSource}

object MainProducer{
  val dataSource: DataSource = new OandaDataSource
  val TOPIC = "raw_weather"

  def main(args: Array[String]): Unit = {
    val props = new Properties()
    props.put("bootstrap.servers", "127.0.01:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](props)

    val arrays: Array[String] = getDataSource.data.split(";")
    for(i <- arrays.indices){ producer.send(new ProducerRecord(TOPIC, "key", arrays(i))) }
    producer.close()
  }

  def getDataSource: DataSource = {
    Util.getPropertie("data-source") match {
      case "oanda"  => new OandaDataSource
      case "yahoo"  => new YahooDataSource
    }
  }
}
