package yaruliy.run
import kafka.serializer.StringDecoder
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka.KafkaUtils
import yaruliy.model.Oanda.RawOandaData

object RunStream {
  def main(args: Array[String]): Unit = {
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)
    val kafkaTopicRaw = "raw_weather"
    val kafkaBroker = "127.0.01:9092"
    val sparkConf = new SparkConf().setAppName("Yaruliy")
    sparkConf.setIfMissing("spark.master", "local[5]")
    val streamContext = new StreamingContext(sparkConf, Seconds(2))
    val topics: Set[String] = kafkaTopicRaw.split(",").map(_.trim).toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> kafkaBroker)

    val rawOandaStream =
      KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](streamContext, kafkaParams, topics)
    val parsedWeatherStream: DStream[RawOandaData] = ingestStream(rawOandaStream)

    parsedWeatherStream.foreachRDD(rdd => {
      rdd.foreach(oanda => {
        println(oanda)
        /*if(oanda.baseCurrency.equals("BTC")){
          println("The price of bitcoin is " + oanda.purchase)
        }*/
      })
    })

    streamContext.start()
    streamContext.awaitTermination()
    streamContext.stop()
  }

  def ingestStream(rawWeatherStream: InputDStream[(String, String)]): DStream[RawOandaData] = {
    val parsedWeatherStream = rawWeatherStream.map(_._2.split(",")).map(RawOandaData(_))
    parsedWeatherStream
  }
}
