package yaruliy.model

object Oanda{
  @SerialVersionUID(1L)
  sealed trait Model extends Serializable

  case class RawOandaData(baseCurrency: String,
                          currency: String,
                          date: String,
                          purchase: Double,
                          sale: Double) extends Model{
    override def toString: String =
      baseCurrency + " -> " + currency + ", Date: " + date + "; " + " {P: " + purchase + "; S: " + sale + "}"
  }

  object RawOandaData {
    def apply(array: Array[String]): RawOandaData = {
      RawOandaData(
        baseCurrency = array(0),
        currency = array(1),
        date = array(2),
        purchase = array(3).toDouble,
        sale = array(4).toDouble
      )
    }
  }
}
